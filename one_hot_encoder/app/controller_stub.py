from datetime import datetime
import pickle
import os
from pyspark.sql import DataFrame
from .mongo import MongoPersistenceManager

class ControllerStub:

    def __init__(self):
        pass

    def pipeline_component_started(self, run_id, component_name):
        self.report_pipeline_status(run_id, component_name,
                                    {"status": {"status": f"component started {component_name}"}})
        print('Controller_stub - pipeline_component_started')

    def pipeline_component_terminated(self, run_id, component_name):
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component terminated"}})
        os._exit(0)

    def pipeline_component_paused(self, run_id, component_name,
                                  component_state):
        # delete any state file
        print(f'In {self.__class__.__name__}.pipeline_component_paused, \
        {component_name} and {component_state}')
        self.clean_state(run_id, component_name)
        self.save_state(run_id, component_name, component_state)

        # for the run, update the metadata component
        # name at which pipeline was paused
        pm = MongoPersistenceManager()
        print(f'Paused run_id: {run_id} at {component_name}')
        pm.update('runs', {'run_name' : run_id}, \
            {'paused_at' : component_name}, upsert=True)

        self.report_pipeline_status(run_id, component_name,
                                    {"status": {"status": "component paused"}})

    def pipeline_component_restarted(self, run_id, component_name):
        component_state = self.load_state(run_id, component_name)
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component restarted"}})
        return component_state

    def pipeline_component_completed(self, run_id, component_name):
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component completed"}})

    def get_pipeline_run_status(self, run_id):
        persistent_manager = MongoPersistenceManager()
        runs = persistent_manager.find('runs', {'run_name' : run_id})
        status = 'IDLE'
        if runs:
            status = runs[0].get('run_status', 'IDLE')
        return status

    def update_pipeline_run_status(self, run_id, status):
        persistent_manager = MongoPersistenceManager()
        persistent_manager.update('runs', {'run_name': run_id}, 
                {'run_status' : status}, upsert=True)

    def report_pipeline_status(self, run_id, component_name, status):
        now = datetime.now()
        curr_time = now.strftime("%Y-%m-%d %H:%M:%S")
        # update mongo status with pm
        print(f'reporing status = {status}')
        pm = MongoPersistenceManager()
        runs = pm.find('runs', {'run_name' : run_id})
        s = []
        if runs:
            s = runs[0].get('status', [])
        else:
            print(f'No status for {component_name}')
        s.append(status)
        pm.update('runs', doc_filter={'run_name' : run_id},
                                obj={'status':s})
        print(
            "Run ID: {}, Time:{}, Component: {}, Status: {}".format(str(run_id),
                                                                    curr_time,
                                                                    component_name,
                                                                    status),
            flush=True)

    def save_state(self, run_id, component_name, state):
        print("Saving state", flush=True)
        file_name = self.get_state_filename(run_id, component_name)
        if isinstance(state, DataFrame):
            path = f'/user/xprops/xpr_gun_spark_ml_metric_and_exp_1_sc/{run_id}' + '.parquet'
            state.write.format("parquet").mode("overwrite").save(path)
        else:
            print(state, flush=True)
            pickle_out = open(file_name, "wb")
            pickle.dump(state, pickle_out)
            pickle_out.close()

    def load_state(self, run_id, component_name):
        print("Loading state", flush=True)
        state_file = self.get_state_filename(run_id, component_name)
        state = None
        if os.path.exists(state_file):
            pickle_in = open(state_file, "rb")
            state = pickle.load(pickle_in)
            pickle_in.close()
            print(state, flush=True)
        return None

    def get_state_filename(self, run_id, component_name):
        return f'/user/xprops/xpr_gun_spark_ml_metric_and_exp_1_sc/{run_id}' + '.parquet'

    def clean_state(self, run_id, component_name):
        state_file = self.get_state_filename(run_id, component_name)
        if os.path.exists(state_file):
            print(f'State file exists: {state_file}')
            os.remove(state_file)
        else:
            print(f'State file doesnt exists: {state_file}')