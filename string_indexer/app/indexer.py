
import os

from pyspark.ml.feature import StringIndexer
from one_hot_encoder.app.abstract_component import AbstractSparkPipelineEstimator


class CustomStringIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, run_id, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={self.name} with run_id {run_id}', flush=True)
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, run_id)

    def _fit(self, dataset):
        self.state = dataset
        args = [self.run_id]
        self.start(*args)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        self.completed()
        print(f'Completed component: {self.name}', flush=True)
        return model

class LabelIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    
    def __init__(self, name, run_id, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={self.name} with run_id {run_id}', flush=True)
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, run_id)
    
    def _fit(self, dataset):
        self.state = dataset
        args = [self.run_id]
        self.start(*args)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        self.completed()
        print(f'Completed component: {self.name}', flush=True)
        return model