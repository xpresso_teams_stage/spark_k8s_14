__author__ = "xpresso.ai"
import argparse
import os 
import sys

if __name__ == "__main__":
    print('Running...', flush=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('--arg-name-1', type=str, default='arg-value-1',
                        help='args 1 help')
    parser.add_argument('--arg-name-2', type=str, default='arg-value-2',
                        help='arg 2 help')
    parser.add_argument('--run_name', type=str, default='run_name-value',
                        help='run_name help')
    args = parser.parse_args()
    print('='*40)
    print('args')
    if args:
        print(args)
    else:
        print('"args" empty !!!')
    print('='*40)

    args = sys.argv[1:]
    if args:
        run_id = args[0]
    print(f'All: {args}', flush=True)
    print(run_id, flush=True)