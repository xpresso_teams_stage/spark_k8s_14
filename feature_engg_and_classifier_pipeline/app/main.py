__author__ = 'Xpresso'

import importlib
import time
import os
import sys
import time
import argparse


if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark

from string_indexer.app.indexer import CustomStringIndexer
from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.sql.types import IntegerType

from string_indexer.app.indexer import CustomStringIndexer
from string_indexer.app.indexer import LabelIndexer
from one_hot_encoder.app.encoder import CustomerOneHotEncoderEstimator
from vector_assembler.app.assembler import CustomVectorAssembler

from one_hot_encoder.app.abstract_component import XprPipeline
from one_hot_encoder.app.mongo import MongoPersistenceManager

if __name__ == "__main__":
    # DO NOT CHANGE
    parser = argparse.ArgumentParser()
    parser.add_argument('--run_name', type=str, default='runname-value',
                        help='runname help')
    parser.add_argument('--arg-name-1', type=str, default='add',
                        help='rg-name-1 help')
    parser.add_argument('--arg-name-2', type=str, default='add',
                        help='rg-name-2 help')
    args = parser.parse_args()
    print('='*40)
    print('args')
    if args:
        print(args)
    else:
        print('"args" not found...!!!!')
    print('='*40)
    print('Exiting...')

    args = sys.argv[1:]
    if args:
        run_id = args[0]
    print(run_id, flush=True)
    
    pm = MongoPersistenceManager()
    res = pm.find('runs', {'run_name': run_id})
    run_status = None
    if res:
        run_status = res[0].get('run_status')
        restore_path = res[0].get('restore_path')
        print(f'Found run {run_id} with status={run_status} and restore_path={restore_path}', flush=True)

    spark = SparkSession \
        .builder \
        .appName(run_id) \
        .getOrCreate()

    # data input location in HDFS
    # data input to be changef by developer
    HDFS_BASE_PATH="hdfs://172.16.1.81:8020/user/xprops/dataset/h1b/h-1b-visa"
    HDFS_INPUT_TRAIN_DATA_LOCATION= HDFS_BASE_PATH + "/input"
    HDFS_OUTPUT_DATA_LOCATION=HDFS_BASE_PATH + "/output-"

    # read run status at the beginning - to avoid the dependency on spark session in worker nodes.
    data_df = spark.read.load(HDFS_INPUT_TRAIN_DATA_LOCATION, format="csv", inferSchema="true", header="true", mode="DROPMALFORMED")
    print('Schema to start with is...', flush=True)
    data_df.printSchema()
    # DO NOT CHANGE


    # figure out which features/cols to work with
    categoricalColumns = ['EMPLOYER_NAME', 'JOB_TITLE', 'FULL_TIME_POSITION', 'WORKSITE', 'YEAR']
    #categoricalColumns = ['EMPLOYER_NAME'] # for short job
    numericColumns = ['PREVAILING_WAGE']

    # type casting if required
    df = data_df.withColumn("PREVAILING_WAGE", data_df["PREVAILING_WAGE"].cast(IntegerType()))
    cols = df.columns
    df.printSchema()

    # decide on pipeline with stages and add them to pipeline_stages
    pipeline_stages = []
    for categoricalCol in categoricalColumns:
        stringIndexer = CustomStringIndexer(categoricalCol+'-indexer', run_id, inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
        encoder = CustomerOneHotEncoderEstimator(categoricalCol+'-encoder', run_id, inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
        pipeline_stages += [stringIndexer, encoder]

    label_stringIdx = LabelIndexer('labelindexer', run_id, inputCol = 'CASE_STATUS', outputCol = 'label')
    pipeline_stages += [label_stringIdx]

    assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
    assembler = CustomVectorAssembler('assembler', run_id, inputCols=assemblerInputs, outputCol="features")
    # assembler.setParams(handleInvalid="skip")
    pipeline_stages += [assembler]
    # pipeline stages design complete

    # generate pipeline and run with the dataframe
    pipeline = XprPipeline('mypipeline', spark, run_id, stages = pipeline_stages)
    pipelineModel = pipeline.fit(df)
    df = pipelineModel.transform(df)
    selectedCols = ['label', 'features'] + cols
    df = df.select(selectedCols)
    df.printSchema()

    # split the data with evaluation
    train, test = df.randomSplit([0.8, 0.2], seed = 2018)
    print("n\nTraining Dataset Count: " + str(train.count()))
    print("Test Dataset Count: " + str(test.count()))


    rf = RandomForestClassifier(featuresCol = 'features', labelCol = 'label')
    rfModel = rf.fit(train)
    predictions = rfModel.transform(test)
    predictions.printSchema()

    evaluator = MulticlassClassificationEvaluator()
    print("\n\nAccuracy: " + str(evaluator.evaluate(predictions, {evaluator.metricName: "accuracy"})))

    spark.stop()
